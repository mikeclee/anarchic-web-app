
### Checklist

[ ] Create manifest
[ ] Link the manifest in App/page
[ ] Create service work
[ ] Link sw in App/page

### Useful Links

* [How to create PWA from scratch | by Vaadin | 2018-07](https://www.youtube.com/watch?v=nr-_rLMDdeI)
* [Web App Manifest Generator | Firebase](https://app-manifest.firebaseapp.com/)
* [Medium Top PWA Tutorials | 2018-08](https://medium.com/quick-code/top-tutorials-to-learn-progressive-web-app-pwa-57bdf06af328)
* [Google Workbox library for PWA](https://developers.google.com/web/tools/workbox/)
