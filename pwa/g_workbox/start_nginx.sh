#!/usr/bin/env bash

docker run --rm -d -p 8080:80 \
    -v `pwd`/public:/usr/share/nginx/html \
    --name wip-web \
    nginx:alpine