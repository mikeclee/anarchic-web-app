importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js');

console.log('Hello from service-worker.js');

// check workbox instance
if (typeof workbox != "undefined") {
    const PRECACHELIST = [
        '.'
        // { url: '/index.html', revision: 'a1235' }
    ];

    console.log("Workbox loaded");

    // pre-caching feature
    workbox.precaching.precacheAndRoute(PRECACHELIST);
    // routing feature
    // .js are network first to get the latest version
    workbox.routing.registerRoute(
        '/.js$/',
        new workbox.strategies.NetworkFirst()
    );
    // .css are cache first with update in the background
    workbox.routing.registerRoute(
        new RegExp('.*\.css$'),
        new workbox.strategies.StaleWhileRevalidate({
            cacheName: 'css-cache',
        })
    );
    // images are cache only with TTL
    workbox.routing.registerRoute(
        '/\.(?:png|jpg|jpeg|svg|gif)$/',
        new workbox.strategies.CacheFirst({
            cacheName: 'image-cache',
            plugins: [
                new workbox.expiration.Plugin({
                    maxEntries: 20,
                    maxAgeSeconds: 7 * 24 * 60 * 60
                })
            ]
        })
    );

} else {
    console.log("miss....");
}