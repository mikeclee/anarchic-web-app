Progressive Web App w/ Workbox
==============================

Learning about Google Workbox tool for PWA

([Google Workbox Homepage](https://developers.google.com/web/tools/workbox/))

### Pre-Caching

To enable application working offline and for assets that will not be refreshing frequently, precaching the PWA function to use.

* Use Webpack to setup ([Workbox Webpack doc](https://developers.google.com/web/tools/workbox/guides/precache-files/webpack))
   
     `npm install workbox-webpack-plugin --save-dev`

     ```
        const WorkboxPlugin = require('workbox-webpack-plugin');

        module.exports = {
        // Other webpack config...

        plugins: [
            // Other plugins...

            new WorkboxPlugin.InjectManifest({
            swSrc: './src/sw.js',
            })
        ]
        };
     ```

     The process shoudl inject into the **swSrc** with something similar to `workbox.precaching.precacheAndRoute(self.__precacheManifest || []);`