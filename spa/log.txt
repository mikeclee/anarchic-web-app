Angular requires Node.js version 8.x or 10.x.

npm install angular-in-memory-web-api --save

ng new <app_name>
ng generate component <component>
ng generate service <service>
ng generate module app-routing --flat --module=app

