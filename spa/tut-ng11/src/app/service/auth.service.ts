import { Injectable } from '@angular/core';
import { HttpClient  } from "@angular/common/http";

import * as moment from 'moment';

import { User } from "@model/user.model";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient
  ) { }

  login(email: string, password: string) {
    return this.http.post<User>('/api/login', {email, password});
      // .do(res => this.setSession)
      // .shareReplay();
  }

  private setSession(authResult: any) {
    const expiresAt = moment().add(authResult.expiresIn, 'second');

    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem('expires_in', JSON.stringify(expiresAt.valueOf()));
  }

  logout() {
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_in');
  }

  public isLoggedIn() {
    return moment().isBefore(this.getExpiration());
  }

  public isLoggedOut() {
    return !this.isLoggedIn();
  }

  getExpiration() {
    const expiration = localStorage.getItem('expires_in');
    const expiresAt = (expiration !== null ? JSON.parse(expiration) : '');
    return moment(expiresAt);
  }
}
