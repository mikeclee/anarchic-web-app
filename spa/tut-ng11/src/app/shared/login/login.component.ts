import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from "@service/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authSvc: AuthService,
    private router: Router
  ) {
    this.form = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  ngOnInit(): void {
  }

  login(): void {
    const inputs = this.form.value;

    if (inputs.email && inputs.password) {
      this.authSvc.login(inputs.email, inputs.password)
        .subscribe(
          () => {
            console.log('User is logged in');
            alert('User is logged in');
            this.router.navigateByUrl('/');
          }
        )
    }
  }

}
