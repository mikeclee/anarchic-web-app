- [Angular 11](#angular-11)
  - [Milestones](#milestones)

Angular 11
==========

Reference: [Official Blog on Release (2020-11-11)](https://blog.angular.io/version-11-of-angular-now-available-74721b7952f7)

## Milestones

**Operation Byelog**

All issues have been triaged in the 3 monorepos.

New Angular team commitment to triage new issues within 2 weeks of report.

## Major changes

### Automatic inline Fonts

### Component Test Harness

### Improved Reporting and Logging

### Language Service

Moving from existing View Engine serivce to Ivy-based next generation language service.

### Update HMR (Hot Module Replacement) Support

### Faster Build

### Webpack 5 support testing
