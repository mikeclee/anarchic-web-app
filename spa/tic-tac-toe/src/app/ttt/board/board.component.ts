import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {

  @Input()
  squares: Array<any>;

  @Output()
  squareClick = new EventEmitter<number>();

  constructor() {
    if (this.squares === undefined) {
      this.squares = Array(9).fill(null);
    }
  }

  ngOnInit(): void {
  }

  handleSquareClick(squareIndex):void {
    this.squareClick.emit(squareIndex);
  }
}
