import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';

@Component({
  selector: 'app-square',
  templateUrl: './square.component.html',
  styleUrls: ['./square.component.scss']
})
export class SquareComponent implements OnInit {

  @Input()
  value: String;

  @Output()
  squareClick = new EventEmitter<Object>();

  constructor() { }

  ngOnInit(): void {
  }

  handleClick(event) {
    this.squareClick.emit({
      original: event,
      value: this.value,
    })
  }
}
