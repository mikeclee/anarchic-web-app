import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {

  history = [Array(9).fill(null)];
  isGameOver = false;
  moves = [];
  status = '';
  stepNumber = 0;
  xIsNext = true;

  constructor() { }

  ngOnInit(): void {
    this.updateStatus();
  }

  calculateWinner(squares: Array<any>): String {
    const lines = [
      [0,1,2],
      [3,4,5],
      [6,7,8],
      [0,3,6],
      [1,4,7],
      [2,5,8],
      [0,4,8],
      [2,4,6],
    ];
    let winner = null;
    lines.forEach(value => {
      const [first, second, last] = value;
      if (squares[first] !== null
        && squares[first] === squares[second]
        && squares[first] === squares[last]
      ) {
        winner = squares[first];
        this.isGameOver = true;
        return;
      }
    });

    return winner;
  }

  getCurrentSquares(): Array<any> {
    return this.history[this.stepNumber].slice();
  }

  handleRestart():void {
    this.history = [Array(9).fill(null)];
    this.isGameOver = false;
    this.stepNumber = 0;
    this.xIsNext = true;
    this.updateStatus();
  }

  handleSquareClick(squareIndex):void {
    const history = this.history.slice(0, this.stepNumber + 1);
    const squares = history[history.length - 1].slice();
    if (squares[squareIndex] !== null || this.calculateWinner(squares)) {
      return;
    }
    // update the board
    this.isGameOver = false
    squares[squareIndex] = (this.xIsNext ? 'X' : 'O');
    history.push(squares);
    this.history = history;
    this.stepNumber++;
    this.xIsNext = !this.xIsNext;
    this.updateStatus();
  }

  jumpTo(step: number):void {
    // only jump if the step parameter is valid
    if (step >=0 && step < this.history.length) {
      this.stepNumber = step;
      this.xIsNext = (step % 2 === 0);
      this.updateStatus();
    }
  }

  updateStatus():void {
    const squares = this.getCurrentSquares();
    const winner = this.calculateWinner(squares);
    this.status = (winner === null)
      ? 'Next Player: ' + (this.xIsNext ? 'X' : 'O')
      : 'Winner: ' + winner;
    // create the moves information
    const moves = new Array<Object>();
    this.history.map((_, index) => {
      moves.push({
        moveIndex: index,
        description: (index
          ? 'Go to move #' + index
          : 'Go to game start')
      })
    });
    this.moves = moves;
  }

}
