import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service'

/**
 * Annotation for Angular component; Metadata decorator
 */
@Component({
  selector: 'app-heros',
  templateUrl: './heros.component.html',
  styleUrls: ['./heros.component.css']
})
export class HerosComponent implements OnInit {

  heros: Hero[];

  constructor(private heroService : HeroService) { }

  /**
   * Lifecycle hook - after component is created by Ng
   */
  ngOnInit() {
    this.heroService.getHeros()
      .subscribe(heros => this.heros = heros);
  }

  add(name: string):void {
    name = name.trim();
    if (!!name) {
      this.heroService.addHero({ name } as Hero)
        .subscribe(
          hero => {
            this.heros.push(hero)
          }
        )
    }
  }

  delete(hero: Hero):void {
    this.heros = this.heros.filter(h => h !== hero);
    this.heroService.deleteHero(hero).subscribe();
  }
}
