import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Hero } from './hero'

import { MessageService } from './message.service'

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  private heroesUrl = 'api/heros';

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    }
  }

  private log(message: string) {
    this.messageService.add(`HeroService: ${message}`);
  }

  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) { }

  addHero(hero: Hero): Observable<Hero> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post<Hero>(
        this.heroesUrl,
        hero,
        httpOptions)
      .pipe(
        tap((newHero: Hero) => this.log(`add hero with ID=${newHero.id}`)),
        catchError(this.handleError<Hero>('add Hero'))
      )
  }

  /**
   * Flexible to delete by Hero instance or Hero ID directly
   * @param hero
   */
  deleteHero(hero: Hero | number): Observable<Hero> {
    const id = typeof hero === 'number' ? hero : hero.id;
    const url = `${this.heroesUrl}/${id}`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.http.delete<Hero>(url, httpOptions)
      .pipe(
        tap(_ => this.log(`delete hero with ID: ${id}`)),
        catchError(this.handleError<Hero>('delete Hero'))
      );
  }

  getHero(id: number): Observable<Hero> {
    const url = `${this.heroesUrl}/${id}`;
    return this.http.get<Hero>(url)
      .pipe(
        tap(_ => this.log(`fetch hero ID: ${id}`)),
        catchError(this.handleError<Hero>(`getHero id=${id}`))
      );
  }

  getHeros(): Observable<Hero[]> {
    return this.http.get<Hero[]>(this.heroesUrl)
      .pipe(
        tap(_ => this.log('fetched heros...')),
        catchError(this.handleError<Hero[]>('getHeros', []))
      );
  }

  searchHero(search: string): Observable<Hero[]> {
    this.log(`searching ${search}`);
    if (!search.trim()) {
      // empty search
      this.log('Empty')
      return of([]);
    }
    const url = `${this.heroesUrl}/?name=${search}`;
    return this.http.get<Hero[]>(url)
      .pipe(
        tap(_ => this.log(`found heros matching ${search}`)),
        catchError(this.handleError<Hero[]>('search Heros', []))
      );
  }

  updateHero(hero: Hero):Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    const url = `${this.heroesUrl}/${hero.id}`;

    return this.http.put(url, hero, httpOptions)
      .pipe(
        tap(_ => this.log(`update hero with ID = ${hero.id}`)),
        catchError(this.handleError<any>(`update Hero`))
      );
  }

}
