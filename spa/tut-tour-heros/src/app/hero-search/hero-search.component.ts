import { Component, OnInit } from '@angular/core';

import { Observable, Subject } from 'rxjs';

import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

import { Hero } from '../hero';
import { HeroService } from '../hero.service';

@Component({
  selector: 'app-hero-search',
  templateUrl: './hero-search.component.html',
  styleUrls: ['./hero-search.component.css']
})
export class HeroSearchComponent implements OnInit {

  heros$: Observable<Hero[]>;
  private searchTerm = new Subject<string>();

  constructor(private heroService: HeroService) { }

  ngOnInit(): void {
    const keyWaitTime = 300;
    this.heros$ = this.searchTerm.pipe(
      // ms wait for keystroke before considering the term
      debounceTime(keyWaitTime),
      // ignore duplicates
      distinctUntilChanged(),
      // switch to new Observable when term change
      switchMap((term: string) => this.heroService.searchHero(term))
    )
  }

  /**
   * build search string
   */
  search(term: string): void {
    this.searchTerm.next(term);
  }

}
