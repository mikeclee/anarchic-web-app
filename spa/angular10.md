Angular 10
==========

Reference: [Official Blog on Release](https://blog.angular.io/version-10-of-angular-now-available-78960babd41)

[[toc]]

## Major Changes

1. warning on CommonJS imports because of the possible performance impact
1. optional `--strict` flag for `new` command
1. dependency version update - keep up with ecosystem
1. updated browser support list (**note** default is no longer ES5 build.  Need to update `.browserslistrc` as needed)
1. `tsconfig.*.json` is in multiple parts with extend relationship

## Tutorial

1. `ng add @angular/material`
