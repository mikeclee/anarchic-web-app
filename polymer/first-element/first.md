
(Base on [freeCodeCamp](https://medium.freecodecamp.org/how-to-write-simple-modern-javascript-apps-with-webpack-and-progressive-web-techniques-a30354eab214))

## Git Reference

[Essential Git Handbook](https://medium.freecodecamp.org/the-essential-git-handbook-a1cf77ed11b5)

## Sequence Diagram

(Quick online preview at [WebSequenceDiagrams](https://www.websequencediagrams.com/))

```mermaid
sequenceDiagram
    Alice->Bob: Authentication Request
    note right of Bob: Bob thinks about it
    Bob->Alice: Authentication Response
```

## Build Process

```
npm init
npm install webpack --save-dev && npm install webpack-cli --save-dev
```

Use [Webpack](https://webpack.js.org/) ; reference [A tale of Webpack 4 and how to finally configure it in the right way. Updated. (2018-04)](https://hackernoon.com/a-tale-of-webpack-4-and-how-to-finally-configure-it-in-the-right-way-4e94c8e7e5c1)

Configure `argv` to allow for `--mode` configuration

### Features

* ES6 & Dynamic Imports support
* Babel is a popular transpiler which is there
* babelrc
     ```
     {
        "presets": ["@babel/preset-env"],
        "plugins": ["@babel/plugin-syntax-dynamic-import"]
    }
    ```
* sass-loader
* css-loader
* style-loader
* MiniCssExtractPlugin
* clean-webpack-plugin: For cleanup of the dist folder contents.
* compression-webpack-plugin: For gzipping the dist folder file contents.
* copy-webpack-plugin: For copying static assets, files or resources from application source to dist folder.
* html-webpack-plugin: For creating an index.html file in the dist folder.
* webpack-md5-hash: For hashing application source files in the dist folder.
* webpack-dev-server: For running a local development server.