**Table of Content**
- [Google Polymer Project](#google-polymer-project)
  - [Tutorials](#tutorials)
    - [Open Web Components](#open-web-components)
    - [Vaadin Todos](#vaadin-todos)
  - [Reference](#reference)


Google Polymer Project
======================

## Tutorials

### Open Web Components

1. `npm init @open-wc` to start/apply with **open-wc** template
1. setup the script to start with an entry HTML
    1. add component to the page with:
        ```
        <script type="module" src='./src/components/my-element.js'></script>
        ```
1. [Optional] setup a quick server to test during development
    1. `npm i --save-dev @web/dev-server`
    1. run the application with `web-dev-server --node-resolve --watch --esbuild-target auto`

### Vaadin Todos

Reference: [Vaadin LitElement Setup Tutorial](https://vaadin.com/learn/tutorials/lit-element/starting-a-lit-element-project)

1. create **vaadin-todo** folder and navigate to it with `mkdir vaadin-todo && cd vaadin-todo` 
1. `npm init @open-wc` to start/apply with **open-wc** template to existing (current folder)
1. `npm install lit-element` to update lit-element to latest version
1. create new _src/views/todo-view.js_ with the new **todo-view** custom Element, add the `<todo-view></todo-view>` element on the _index.html_ page

## Reference 

* [Official LitElement Site](https://lit-element.polymer-project.org)
* [Official Modern Web](https://modern-web.dev/)
* [Official Open Web Components](https://open-wc.org/)
