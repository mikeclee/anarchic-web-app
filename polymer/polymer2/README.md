# \<p2-first\>


## Workflow

1. install the official CLI tools - `npm install polymer-cli`
1. initialize the project with `polymer init`
    1. select `polymer-2-element` to start from scratch
    1. provide a component name
1. Polymer 2 uses bower as the tool to dependency manager
    1. install bower CLI tool with - `npm install bower`
    1. run `bower install` to get all dependencies
1. `polymer serve` to run component locally
1. install **iron-icon** as one of the dependency
1. property can have these attributes:
    * type
    * value - default value
    * notify - dispatch property change event - handler, or 2-way binding
    * reflectToAttribute - DOM value propagation  

### Layout

* dom-module tag is the root of the Polymer 2 shadow dom
* dom-module's ID attribute should match the component name
* child element **template** define the actual shadow dom structure and styling
* child element **template/style** define the scoped style for the shadow dom
* use of `:host` in **style** tag allows styling of the custom element, or shadow dom wrapper
* **script** tag is used to define the element logic
    * must create a class that extends **Polymer.Element**
    * must register the element with `customElements.define(<tag>, <className>)

## Install the Polymer-CLI


First, make sure you have the [Polymer CLI](https://www.npmjs.com/package/polymer-cli) installed. Then run `polymer serve` to serve your element locally.

## Viewing Your Element

```
$ polymer serve
```

## Running Tests

```
$ polymer test
```

Your application is already set up to be tested via [web-component-tester](https://github.com/Polymer/web-component-tester). Run `polymer test` to run your application's test suite locally.
