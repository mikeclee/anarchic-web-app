import { html } from 'lit-html';
import '../src/my-todo.js';

export default {
  title: 'my-todo',
};

export const App = () =>
  html`
    <my-todo></my-todo>
  `;
