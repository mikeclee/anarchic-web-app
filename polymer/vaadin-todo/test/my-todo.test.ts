import { html, fixture, expect } from '@open-wc/testing';

import {MyTodo} from '../src/MyTodo.js';
import '../src/my-todo.js';

describe('MyTodo', () => {
  let element: MyTodo;
  beforeEach(async () => {
    element = await fixture(html`
      <my-todo></my-todo>
    `);
  });

  it('renders a h1', () => {
    const h1 = element.shadowRoot!.querySelector('h1')!;
    expect(h1).to.exist;
    expect(h1.textContent).to.equal('My app');
  });

  it('passes the a11y audit', async () => {
    await expect(element).shadowDom.to.be.accessible();
  });
});
