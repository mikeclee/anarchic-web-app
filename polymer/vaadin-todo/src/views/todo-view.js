import { LitElement, html } from 'lit-element';
import { repeat } from "lit-html/directives/repeat";

const EVENT_ENTER = 'Enter';

export const VisibilityFilters = {
  SHOW_ALL: 'All',
  SHOW_ACTIVE: 'Active',
  SHOW_COMPLETED: 'Completed'
};

class TodoView extends LitElement {

  static get properties() {
    return {
      task: { type: String },
      todos: { type: Array },
      filter: { type: String }
    }
  }

  constructor() {
    super();

    this.todos = [];
    this.filter = VisibilityFilters.SHOW_ALL;
  }

  render() {
    return html`
    <p>todo-view</p>
    <div class="task-input">
      <input type="text"
        .value="${this.task || ''}"
        placeholder="New task"
        @change="${this.updateTask}"
        @keyup="${this.handleTaskShortcut}"
      >
      <input type="button"
        value="Add Todo"
        @click="${this.addTodo}"
      >
    </div>
    <span @click="${this.handleFilterSelect}">
      <input type="radio" id="fShowAll"
        name="iFilter"
        value="Show All"
        @click="${this.handleFilterSelect}"
        ?checked="${this.filter === VisibilityFilters.SHOW_ALL}"
      />
      <label for="fShowAll">Show All</label>
      <input type="radio" id="fShowActive"
        name="iFilter"
        value="Active"
      />
      <label for="fShowActive">Show Active</label>
      <input type="radio" id="fShowCompleted"
        name="iFilter"
        value="Completed"
      />
      <label for="fShowCompleted">Show Completed</label>
    </span>
    <input type="button"
      value="Clear Completed"
      @click="${this.handleClearCompleted}"
    />
    <hr>
    ${
      repeat(this.getFilteredTodos(), (item) => item.todo, (item, index) =>
        html`
          <div>
            <p>${item.todo}</p>
            <input type="checkbox"
              value="hi"
              .checked=${item.completed}
              @change="${(e) => { this.handleTaskStatus(item, e.target.checked); }}"
            />
            ${JSON.parse(item.completed)}
          </div>
        `
      )
    }
    `;
  }

  addTodo(event) {
    console.log(event);
    console.log(event.target);
    if (this.task === '' || this.task === undefined) {
      alert('Please add Task description first...');
      return;
    }
    this.newTodo(this.task);
    this.task = '';
  }

  newTodo(todoTask) {
    var newTodo = null;
    if (todoTask !== undefined
      && todoTask !== ''
    ) {
      newTodo = {
        todo: todoTask,
        completed: false
      }
      this.todos.push(newTodo);
    }
    return newTodo;
  }

  getFilteredTodos() {
    switch (this.filter) {
      case VisibilityFilters.SHOW_ACTIVE:
       return this.todos.filter(item => !item.completed);
      case VisibilityFilters.SHOW_COMPLETED:
       return this.todos.filter(item => item.completed);
      default:
        return this.todos.slice();
    }
    // return this.todos.filter((item) => {
    //   return (
    //     (this.filter === VisibilityFilters.SHOW_ACTIVE
    //       && !item.completed)
    //     || (this.filter === VisibilityFilters.SHOW_COMPLETED
    //       && item.completed)
    //     || (this.filter === VisibilityFilters.SHOW_ALL)
    //   )
    // });
  }

  handleClearCompleted(event) {
    const newTodos = this.todos.filter((item) => {
      return !item.completed;
    });
    this.todos = newTodos;
  }

  handleFilterSelect(event) {
    event.stopPropagation();
    if (!!event.target.id) {
      switch (event.target.id) {
        case 'fShowActive':
          this.filter = VisibilityFilters.SHOW_ACTIVE;
          break;
        case 'fShowCompleted':
          this.filter = VisibilityFilters.SHOW_COMPLETED;
          break;
        default:
          this.filter = VisibilityFilters.SHOW_ALL;
      }
    }
  }

  handleTaskStatus(todoItem, isCompleted) {
    const newTodos = this.todos.map((item) => {
      return (item === todoItem
        ? {
          todo: item.todo,
          completed: isCompleted
        }
        : item
      );
    });
    this.todos = newTodos;
    console.log(this.todos);
  }

  handleTaskShortcut(event) {
    if (event.key === EVENT_ENTER) {
      this.addTodo(event);
    }
  }

  updateTask(event) {
    this.task = event.target.value.trim();
  }

}

customElements.define('todo-view', TodoView);
