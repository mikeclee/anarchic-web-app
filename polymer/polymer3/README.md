# \<p3-element\>

## Workflow

1. install the official CLI tools - `npm install polymer-cli`
1. initialize the project with `polymer init`
    1. select `polymer-3-element` to start from scratch
    1. provide a component name
    1. create the **package.json**
1. Polymer 3 move to NPM as the depenedency manager
    1. [Optional] add new dependency for tutorial - `npm install @polymer/iron-icon` 
          * add lower dependency - `npm install @polymer/iron-icons`  
    1. run `npm install` to get all dependencies 

### Layout

* Polymer 3 is pure ES6 using class definitions
* component class should extend from **PolymerElement**
* static **template** needs to defined and return instance of [HTMLTemplateElement](https://developer.mozilla.org/en-US/docs/Web/API/HTMLTemplateElement); the function will create the actual shadow dom structure and styling
* child element **style** in **template**'s returned HTML define the scoped style for the shadow dom
* use of `:host` in **style** allows styling of the custom element, or shadow dom wrapper
* static **properties** define the component's exposed properties
    * property can have these attributes:
        * type - this determine how data is deserialized in HTML
        * value - default value
        * notify - dispatch property change event - handler, or 2-way binding
        * reflectToAttribute - DOM value propagation  
* must register the element with `customElements.define(\<tag\>, \<className\>)
* in general, use the standard addEventListener method to add event listeners imperatively, e.g. `this.addEventListener('click', this.handler.bind(this));`
    * note the passing of context to allow proper instance reference
* [optional] use [ES6 arrow functions](https://hacks.mozilla.org/2015/06/es6-in-depth-arrow-functions/) for quicker function/context definitions
* custom style with use of CSS **var**, e.g. `p3-element { --p3-element-color: red; }`
    * Because custom properties aren't built into most browsers yet, if you use them outside a Polymer element, you need to use a special custom-style tag, e.g. `<custom-style><style>...`
    * Note custom style selector only works at "current" document scope, so it will not match elements inside a showdow-dom
  
## Install the Polymer-CLI

First, make sure you have the [Polymer CLI](https://www.npmjs.com/package/polymer-cli) and npm (packaged with [Node.js](https://nodejs.org)) installed. Run `npm install` to install your element's dependencies, then run `polymer serve` to serve your element locally.

## Viewing Your Element

```
$ polymer serve
```

## Running Tests

```
$ polymer test
```

Your application is already set up to be tested via [web-component-tester](https://github.com/Polymer/web-component-tester). Run `polymer test` to run your application's test suite locally.
