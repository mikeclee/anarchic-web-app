import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import "@polymer/iron-icon";
import "@polymer/iron-icons";

/**
 * `p3-element`
 *
 *
 * @customElement
 * @polymer
 * @demo demo/index.html
 */
class P3Element extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: inline-block;
          color: var(--p3-element-color, currentColor);
        }
        icon-icon {
          fill: var(--icon-toggle-color, rgba(0,0,0,0));
          stroke: var(--icon-toggle-outline-color, currentColor);
        }
        :host([pressed]) iron-icon {
          fill: var(--icon-toggle-outline-color, currentColor);
        }
      </style>
      <h2>Hello and CYA... [[prop1]][[toggleIcon]]!</h2>
      <iron-icon icon="[[toggleIcon]]"></iron-icon>
    `;
  }
  static get properties() {
    return {
      pressed: {
        type: Boolean,
        value: false,
        notify: true,
        reflectToAttribute: true
      },
      prop1: {
        type: String,
        value: 'p3-element',
      },
      toggleIcon: {
        type: String
      }
    };
  }
  constructor() {
    super();
    this.addEventListener('click', this.toggle.bind(this));
  }
  toggle(event) {
    console.log(event);
    this.pressed = !this.pressed
  }
}

window.customElements.define('p3-element', P3Element);
