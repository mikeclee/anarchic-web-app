import { LitElement, html, css } from "lit-element";

class Square extends LitElement {

  static get styles() {
    return css`
      .square {
        background: #fff;
        border: 1px solid #999;
        float: left;
        font-size: 24px;
        font-weight: bold;
        line-height: 34px;
        height: 34px;
        margin-right: -1px;
        margin-top: -1px;
        padding: 0;
        text-align: center;
        width: 34px;
      }

      .square:focus {
        outline: none;
      }
    `;
  }

  static get properties() {
    return {
      value: { type: String }
    }
  }

  render() {
    return html`
      <button
        class="square"
        @click="${this._handleClick}"
       >
        ${this.value ? this.value : html`&nbsp;`}
      </button>
    `;
  }

  _handleClick(event) {
    const squareClick = new CustomEvent('square-click', {
      bubbles: true,
      cancelable: true,
      composed: true,
      detail: this.value,
      origin: event
    });
    this.dispatchEvent(squareClick);
  }

}

customElements.define('ttt-square', Square);
