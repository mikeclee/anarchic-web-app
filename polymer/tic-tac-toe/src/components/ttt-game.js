import { LitElement, css, html } from "lit-element";
import './ttt-board';

class Game extends LitElement {

  static get properties() {
    return {
      history: { type: Array },
      isXNext: { type: Boolean },
      isGameOver: { type: Boolean },
      moves: { type: Array },
      status: { type: String },
      stepNumber: { type: Number }
    }
  }

  static get styles() {
    return css`
      body {
        font: 14px "Century Gothic", Futura, sans-serif;
        margin: 20px;
      }

      ol, ul {
        padding-left: 30px;
      }

      .game {
        display: flex;
        flex-direction: row;
      }

      .game-info {
        margin-left: 20px;
      }

      .status {
        margin-bottom: 10px;
      }
    `;
  }

  constructor() {
    super();
    this._initGame();
  }

  render() {
    return html`
      <div class="game">
        <div class="game-board">
          <ttt-board .squares="${this._getCurrentSquares()}" @board-click="${this._handleBoardClick}"></ttt-board>
        </div>
        <div class="game-info">
          <div class="status">${this.status}</div>
          ${this.moves.length > 1
            ? html`
                <ol>
                  ${this.moves.map(element => {
                    return html`<li>
                    <button @click="${(e) => { this._jumpTo(element.moveIndex); }}">${element.description}</button>
                  </li>`
                  })}
                </ol>`
            : ''
          }
          ${this.isGameOver
            ? html`<button @click="${this._handleRestart}">Restart Game</button>`
            : ''
          }
        </div>
      </div>
    `;
  }

  _calculateWinner(squares) {
    const lines = [
      [0,1,2],
      [3,4,5],
      [6,7,8],
      [0,3,6],
      [1,4,7],
      [2,5,8],
      [0,4,8],
      [2,4,6],
    ];
    let winner = null;
    lines.forEach((element) => {
      const [first, second, last] = element;
      if (squares[first] !== ''
        && squares[first] === squares[second]
        && squares[first] === squares[last]
      ) {
        this.isGameOver = true;
        winner = squares[first];
        return;
      }
    })
    return winner;
  }

  _getCurrentSquares() {
    return this.history[this.stepNumber].slice();
  }

  _handleBoardClick(event) {
    // consume and stop original event
    event.stopPropagation();
    const history = this.history.slice(0, this.stepNumber + 1);
    const currentBoard = history[history.length - 1].slice();
    if (currentBoard[event.detail] !== '' || this._calculateWinner(currentBoard)) {
      return;
    }
    // update the board
    currentBoard[event.detail] = this.isXNext ? 'X' : 'O';
    history.push(currentBoard);
    this.history = history;
    this.isXNext = !this.isXNext;
    this.stepNumber++;
    this.isGameOver = false;
    this._updateStatus();
  }

  _handleRestart() {
    this._initGame();
    this._updateStatus();
  }

  _initGame() {
    this.history = [Array(9).fill('')];
    this.stepNumber = 0;
    this.isXNext = true;
    this.isGameOver = false;
    this.moves = [];
  }

  _jumpTo(step) {
    if (step >= 0 && this.history.length > step) {
      this.stepNumber = step;
      this.isXNext = (step % 2) === 0;
      this._updateStatus();
    }
  }

  _updateStatus() {
    const currentBoard = this._getCurrentSquares();
    const winner = this._calculateWinner(currentBoard);
    this.status = (winner === null)
      ? 'Next Player: ' + (this.isXNext ? 'X' : 'O')
      : 'Winner is ' + winner;
    // create the moves information
    const moves = Array();
    this.history.map((_, index) => {
      moves.push({
        moveIndex: index,
        description: (index
          ? 'Go to move #' + index
          : 'Go to game start')
      });
    });
    this.moves = moves;
  }

}

customElements.define('ttt-game', Game);
