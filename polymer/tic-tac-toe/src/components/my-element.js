import {LitElement, html} from 'lit-element';

class MyElement extends LitElement {

  static get properties() {
    return {
      name: {type: String}
    }
  }

  constructor() {
    super();
    this.name = 'Well';
  }

  render() {
    return html`
      <div>Hello there... ${this.name}</div> 
    `;
  }
}

customElements.define('my-element', MyElement);
