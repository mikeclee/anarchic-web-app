import { LitElement, css, html } from "lit-element";
import './ttt-square';

class Board extends LitElement {

  static get properties() {
    return {
      squares: { type: Array, attribute: true}
    }
  }

  static get styles() {
    return css`
      .board-row:after {
        clear: both;
        content: "";
        display: table;
      }
    `;
  }

  constructor() {
    super();
    if (this.squares === undefined) {
      this.squares = Array(9).fill('');
    }
  }

  render() {
    return html`
      <div class="board-row">
        <ttt-square .value="${this.squares[0]}" @square-click="${(e) => { this._handleSquareClick(e, 0); }}"></ttt-square>
        <ttt-square value="${this.squares[1]}" @square-click="${(e) => { this._handleSquareClick(e, 1); }}"></ttt-square>
        <ttt-square value="${this.squares[2]}" @square-click="${(e) => { this._handleSquareClick(e, 2); }}"></ttt-square>
      </div>
      <div class="board-row">
        <ttt-square value="${this.squares[3]}" @square-click="${(e) => { this._handleSquareClick(e, 3); }}"></ttt-square>
        <ttt-square value="${this.squares[4]}" @square-click="${(e) => { this._handleSquareClick(e, 4); }}"></ttt-square>
        <ttt-square value="${this.squares[5]}" @square-click="${(e) => { this._handleSquareClick(e, 5); }}"></ttt-square>
      </div>
      <div class="board-row">
        <ttt-square value="${this.squares[6]}" @square-click="${(e) => { this._handleSquareClick(e, 6); }}"></ttt-square>
        <ttt-square value="${this.squares[7]}" @square-click="${(e) => { this._handleSquareClick(e, 7); }}"></ttt-square>
        <ttt-square value="${this.squares[8]}" @square-click="${(e) => { this._handleSquareClick(e, 8); }}"></ttt-square>
      </div>
    </div>
    `;
  }

  _handleSquareClick(event, squareIndex) {
    // consume and stop original click event
    event.stopPropagation();
    // emit custom event
    const squareClick = new CustomEvent('board-click', {
      bubbles: true,
      cancelable: true,
      composed: true,
      detail: squareIndex
    });
    this.dispatchEvent(squareClick);
  }

}

customElements.define('ttt-board', Board);
