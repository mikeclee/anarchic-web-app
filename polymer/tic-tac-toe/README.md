**Table of Content**
- [TicTacToe](#tictactoe)
  - [Tutorial](#tutorial)
    - [Step 1](#step-1)
    - [Step 2](#step-2)
    - [Step 3](#step-3)
    - [Step 4](#step-4)
    - [Step 5](#step-5)
    - [Step 6](#step-6)
    - [Step 7](#step-7)
  - [Development server](#development-server)
  - [Code scaffolding](#code-scaffolding)
  - [Build](#build)
  - [Running unit tests](#running-unit-tests)
  - [Running end-to-end tests](#running-end-to-end-tests)
  - [Further help](#further-help)
<hr>
TicTacToe
=========

## Tutorial

This will re-create Official ReactJS TicTacToe with LitElement

1. `npm init @open-wc` to start/apply with **open-wc** template

```mermaid
classDiagram
  Board --> Game: boardClick
  Board "1" <-- "*" Square: squareClick

  Square: +value
  Square: +handleClick(event)
  Board: +squares~Square~
  Board: +handleSquareClick(event)
  Game: +history~Array~~Sqaure~
  Game: +isXNext
  Game: +isGameOver
  Game: +stepNumber
  Game: +jumpTo(step)
  Game: +calculateWinner(squares)
  Game: +getCurrentSquares()
  Game: +handleBoardClick(event)
  Game: +updateStatus()
```

### Step 1

Start the project with `ng new <tic-tac-toe>` to get the default Angular application.

Optional setting choices:
* enable Angular routing
* use scss as style

### Step 2

Familarize the default Angular project files, and quick clean up of the default HTML in **app.component.html**

### Step 3

Create the expected components with `ng generate component <name>` for

* ttt/game
* ttt/board
* ttt/square

### Step 4

Connect the components together with HTML update.

* update component HTML
* update component CSS

### Step 5

Update Square component per design:

* bind the display and component *value*
* create event handling and bubbling 

### Step 6

Update Board component per design:

* create component *squares* property
* create event handling and bubbling 

### Step 7

Update Game component per design:

* create component properties: *history*, *xIsNext*, *isGameOver*, *stepNumber*
* create event handling and bubbling 

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
