// Base react template 
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import App from './App';
import reportWebVitals from './reportWebVitals';

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [{
        squares: Array(9).fill(null),
      }],
      stepNumber: 0,
      xIsNext: true,
    }
  }
  calculateWinner(squares) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (squares[a] !== null && squares[a] === squares[b] && squares[a] === squares[c]) {
        return squares[a];
      }
    }
    return null;
  }
  getCurrentSquares() {
    const history = this.state.history;
    return history[this.state.stepNumber].squares;
  }
  handleSquareClick(i, e) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const newSquares = history[history.length - 1].squares.slice();
    // stop process on winner or pre-filled square
    if (this.calculateWinner(newSquares) || newSquares[i] !== null) {
      return;
    }
    // draw new entry
    newSquares[i] = this.state.xIsNext ? 'x' : 'o';
    this.setState({
      history: history.concat([{
        squares: newSquares
      }]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext,
    });
    // @todo remove logging
    console.log('clicked ' + i);
    console.dir(this.state);
    console.dir(e)
  }
  jumpTo(step) {
    this.setState({
      stepNumber: step,
      xIsNext: (step % 2) === 0,
    });
  }
  render() {
    const currentSquares = this.getCurrentSquares();
    const winner = this.calculateWinner(currentSquares);
    const status = (winner !== null) 
      ? 'Winner: ' + winner
      : 'Next Player: ' + (this.state.xIsNext ? 'X' : 'O');
    // parse out the move
    const moves = this.state.history.map((step, move) => {
      const desc = move ? 
        'Go to move #' + move :
        'Go to game start';
      return (
        <li key={move}>
          <button onClick={() => this.jumpTo(move)}>{desc}</button>
        </li>
      )
    });
    return(
      <div className="game">
        <div className="game-board">
          <Board 
            squares={currentSquares}
            handleSquareClick={(i, e) => this.handleSquareClick(i, e)}
          />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <ol>{moves}</ol>
        </div>
      </div>
    );  
  }
}

class Board extends React.Component {
  constructor(props) {
    super(props);
  }
  renderSquare(i) {
    return (
      <Square 
        value={this.props.squares[i]} 
        handleClick={(e) => this.props.handleSquareClick(i, e)}
      />
    );
  }
  render() {  
    return (
      <div>
        <div className="board-row">
          {this.renderSquare(0)}
          {this.renderSquare(1)}
          {this.renderSquare(2)}
        </div>
        <div className="board-row">
          {this.renderSquare(3)}
          {this.renderSquare(4)}
          {this.renderSquare(5)}
        </div>
        <div className="board-row">
          {this.renderSquare(6)}
          {this.renderSquare(7)}
          {this.renderSquare(8)}
        </div>
      </div>
    );
  }
}

function Square(props) {
  return (
    <button 
      className="square"
      onClick={props.handleClick}
    >
      {props.value}
    </button>  
  )
}

// main render
ReactDOM.render(
  <React.StrictMode>
    {/* <App /> */}
    <Game></Game>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals(console.log);
