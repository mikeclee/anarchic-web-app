ReactJS
=======

## Lesson

### Official ReactJS TicTacToe

1. `npx create-react-app my-app` to create the app using the **create-react-app** CLI

## Reference

* [Official ReactJS](https://reactjs.org/)
* [React Tutorial](https://react-tutorial.app/app.html?id=328)
* [Scotch React To-Do (2016-09)](https://scotch.io/tutorials/create-a-simple-to-do-app-with-react)

### JS Concepts

* const/lt
* Arrays/Objects
* Array methods (filter, find, etc.)
* Spread operator (...)
* array/object destructuring
* import/export
* arrow functions (=>)
* lexical scope
* Promises
