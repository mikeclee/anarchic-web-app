exports.getAll = async (req, res) => {
    res.send([
        {
            "id": "9",
            "type": "CREDIT_CARD",
            "name": "GEM Visa",
            "version": "v2"
        },
        {
            "id": "10",
            "type": "CREDIT_CARD",
            "name": "28 Degrees",
            "version": "v1"
        }
    ]);
};
