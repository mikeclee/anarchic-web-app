const app = require('express')();
// const productRoutes = require('./products/product.routes');
const controller = require('./products/product.controller');

const port = 8085;

// app.use(productRoutes);
app.get('/products', controller.getAll);


app.listen(port, () => {
    console.log(`Producer / Provider started on port: ${port} ...`);
});
