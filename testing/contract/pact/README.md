- [Contract Testing - PACT](#contract-testing---pact)
  - [Tutorial](#tutorial)
    - [Design](#design)
    - [Workflow](#workflow)
      - [Client](#client)
      - [Producer](#producer)
  - [Reference](#reference)

<hr>

Contract Testing - PACT
=======================

## Tutorial

### Design

```mermaid
sequenceDiagram
  Participant client as Consumer
  Participant test as Test
  Participant provider as Provider

  client->>+test: api.getProduct(10)
  test->>+provider: GET /product/10
  provider->>-test: product data
  Note right of test: id: "10"<br>type: "CREDIT_CARD"<br> name: "Degree"<br> version: "v1"
  test-->>-client: result
```
### Workflow

#### Client

1. `npx create-react-app consumer` to create the sample ReactJS App
1. `npm install --save-dev @pact-foundation/pact-node` to install PACT nodejs library
1. `npm install --save-dev nock` to use quick mock for unit-testing
1. [optional] `npm install axios` to use Http lib for making request
1. create **_.env_** file for environment variables
    ```
    REACT_APP_API_BASE_URL=http://localhost:8081
    ```
1. create the service JS
1. create the spec.js as unittest 
1. create the pact.spec.js as the PACT test
1. base on code configuration `...dir: path.resolve(process.cwd(), 'pacts'),...` in the test file, run `npm test` to generate the PACT file, e.g. **pacts/frontendwebsite-productservice.json**


#### Producer

1. `npm init` and `npm install express` to create basic server
1. use express to create the **_getProducts_** endpoint


## Reference

* [Official PACT site](https://docs.pact.io)
* [PACT foundation workshop - JS](https://github.com/pact-foundation/pact-workshop-js)
* [NPM Pact library](https://github.com/pact-foundation/pact-js)
