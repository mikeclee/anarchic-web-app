import API from './api';
import nock from "nock";

describe('API unitest', () => {

  test('get all products', async () => {
    const expectedProducts = [
      {
          "id": "9",
          "type": "CREDIT_CARD",
          "name": "GEM Visa",
          "version": "v2"
      },
      {
          "id": "10",
          "type": "CREDIT_CARD",
          "name": "28 Degrees",
          "version": "v1"
      }
    ];
    // const api = new API();
    console.log(API.url);
    console.log(process.env.REACT_APP_API_BASE_URL);
    nock(API.url)
        .get('/products')
        .reply(200,
          expectedProducts,
            {'Access-Control-Allow-Origin': '*'});
    const actualProducts = await API.getAllProducts();
    expect(actualProducts).toEqual(expectedProducts);
  });

});
