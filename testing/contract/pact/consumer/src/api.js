import axios from 'axios';
import adapter from 'axios/lib/adapters/http';

axios.defaults.adapter = adapter;

export class API {

  constructor(url) {
    if (url === undefined || url === '') {
      url = process.env.REACT_APP_API_BASE_URL;
    }
    if (url !== undefined && url.endsWith("/")) {
      url = url.substr(0, url.length - 1)
    }
    this.url = url;
  }

  /**
   * Create the full endpoint: url + path
   * - logic check to ensure only 1 / between the parts
   *
   * @param {String} path
   * @return {String}
   */
  withPath(path) {
    if (!path.startsWith('/')) {
      path = '/' + path;
    }
    return this.url + path;
  }

  generateAuthToken() {
    return 'Bearer ' + new Date().toISOString();
  }

  async getAllProducts() {
    // Using fetch
    // return await fetch(this.withPath('/products'), {
    //   method: 'GET',
    //   mode: 'no-cors',
    //   credentials: 'same-origin',
    //   headers: {
    //     'Authorization': this.generateAuthToken()
    //   }
    // })
    // .then(response => {console.log(response); return response.json(); })
    // .then(response => response.data);

    // Using Axios
    return axios.get(this.withPath('/products'), {
      headers: {
        'Authorization': this.generateAuthToken()
      }
    })
    .then(result => result.data);
  }

  async getProduct(productId) {
    return fetch(this.withPath('/product/' + productId), {
      headers: {
        'Authorization': this.generateAuthToken()
      }
    })
    .then(response => response.data);
  }
}

export default new API(process.env.REACT_APP_API_BASE_URL);
