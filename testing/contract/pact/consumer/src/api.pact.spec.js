import { API } from "./api";
import path from "path";
import { Pact } from "@pact-foundation/pact";
import { eachLike, like } from "@pact-foundation/pact/dsl/matchers";
// const { Pact } = require("@pact-foundation/pact");

const provider = new Pact({
  consumer: 'FrontendWebsite',
  provider: 'ProductService',
  log: path.resolve(process.cwd(), 'logs', 'pact.log'),
  logLevel: 'info',
  dir: path.resolve(process.cwd(), 'pacts'),
  spec: 2
});

describe('API Pact test', () => {

  beforeAll(() => provider.setup());
  afterEach(() => provider.verify());
  afterAll(() => provider.finalize());

  test('get all products', async () => {
    // setup Pact interactions
    await provider.addInteraction({
      state: 'products exist',
      uponReceiving: 'get all products',
      withRequest: {
        method: 'GET',
        path: '/products',
        header: {
          'Authorization': like('Bearer 2019-01-14T11:34:18.045Z')
        }
      },
      willRespondWith: {
        status: 200,
        header: {
            'Content-Type': 'application/json; charset=utf-8'
        },
        body: eachLike({
            id: "09",
            type: "CREDIT_CARD",
            name: "Gem Visa",
            version: "v2"
        // },
        // {
        //     "id": "10",
        //     "type": "CREDIT_CARD",
        //     "name": "28 Degrees",
        //     "version": "v1"
        })
      }
    });
    // setup SUT
    const api = new API(provider.mockService.baseUrl);
    const expectedProducts =  [
      {
          "id": "09",
          "type": "CREDIT_CARD",
          "name": "Gem Visa",
          "version": "v2"
      // },
      // {
      //     "id": "10",
      //     "type": "CREDIT_CARD",
      //     "name": "28 Degrees",
      //     "version": "v1"
      }
    ];
    const actualProducts = await api.getAllProducts();
    expect(actualProducts).toEqual(expectedProducts);
    // make request to Pact mock server
    // const product = await api.getAllProducts();

    // expect(product).toStrictEqual([
    //     {"id": "09", "name": "Gem Visa", "type": "CREDIT_CARD"}
    // ]);
  });
});
